//AUTOR: Juan Antonio Ceballos
//C1_G18 Misi�nTIC 2022
//Fecha: 9 de Mayo de 2021

Algoritmo registro_vacunacion
	
	Definir edadMin Como Entero // Edad m�nima
	Definir edadMax Como Entero // Edad m�xima
	edadMin <- 50;
	edadMax <- 70;
	Definir rango Como Entero;  // Rango de las edades que se permiten en la jornada de vacunaci�n
	rango <- (edadMax - edadMin)+ 1;
	// Decaraci�n e Inicialzaci�n de variables
	Dimension vacunados[rango,2]; // Matriz en la que se guarda la cantidad de vacunados por edades.
	Definir validacion Como Logico // validaci�n para continuar en el ciclo
	Definir edad Como Entero // Edad de la persona que se va a vacunar, tambi�n se usa para ingresar las edades a la matr�z
	Definir a�oActual Como Entero // Para este ejercicio se toma como 2021
	Definir a�oNacimieto Como Entero // Es el a�o en el que nacio la persona, se necesita para el c�lculo de la edad
	Definir c Como Caracter // variable bandera para ayudar a seguir o salir del ciclo de vacunaciones
	Definir totalVacunados Como Entero // Variables contador para el total de personas vacunadas
	a�oActual <- 2021;
	edad <- edadMin;
	validacion <- Verdadero; // Se  inicializa como verdadero para poder ingresar al ciclo
	
	// Ciclo para ingresar las edades del rango a la primera propiedad del arreglo
	Para i<-1 Hasta rango Hacer
		vacunados[i,1] <- edad;
		edad <- edad + 1;
	FinPara
	
	// Incio de la ejecuci�n del programa
	Mientras validacion=Verdadero Hacer
		Escribir '�Cu�l es tu a�o de nacimiento?';
		Leer a�oNacimieto;
		edad <- a�oActual-a�oNacimieto;
		// conidicional para validar si la edad de la persona corresponde al rango de la jornada de vacunaci�n
		Si edad >= edadMin Y edad <= edadMax Entonces
			totalVacunados <- totalVacunados+1; // sumar la nueva persona al total de vacunados
			Para i<-1 Hasta rango Hacer
				Si edad=vacunados[i,1] Entonces // Confirmar la edad del usuario con la posicion en el arreglo
					vacunados[i,2]<-vacunados[i,2]+1; // Agregar ese vacunado a su respectiva edad
				FinSi
			FinPara
		SiNo
			Escribir 'Lo sentimos, A�n no es tu jornada de vacunaci�n. ';
		FinSi
		
		// Bloque para validar si se contin�a la jornada o ya no hay mas personas
		Escribir 'Conitunar con el siguiente paciente? [S/N]' ;
		Leer nuevo_paciente;
		Si nuevo_paciente='n' O nuevo_paciente='N' Entonces
			validacion <- Falso; // Al cambiar a falso se rompe la condici�n del ciclo 
		FinSi
	FinMientras
	
	// Bloque para imprimir los resultados
	// En este caso me tom� la libertad de moificar la salida.
	// Solo se imprimir� los resultados de las edades que tuvieron personas que se vacunaron.
	Para i<-1 Hasta rango Hacer
		Si vacunados[i,2]<>0 Entonces
			Escribir vacunados[i,1],' a�os - ',vacunados[i,2],' Vacunados';
		FinSi
	FinPara
	Escribir 'Total Vacunados: ',totalVacunados;
FinAlgoritmo
